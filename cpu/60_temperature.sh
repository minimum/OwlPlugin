#!/bin/bash

# Metric value definition:
# -1: Error
# #temperature

# Get highest cpu core temperature.
#   Return float to zabbix.
function cpu_temp() {

    # Init
    sensors > /dev/null 2>&1
    if [ $? -ne 0 ] ; then
        sensors-detect > /dev/null < /dev/null 2>&1 &
        echo -1
        return 1
    fi

    local max_core_temperature=$(sensors | \
                            grep "Core [ \t0-9]*:[ \t]*+[0-9\.]*°C" | \
                            sed "s/Core [ \t0-9]*:[ \t]*+\([0-9\.]*\)°C.*$/\1/g" | \
                            awk 'BEGIN { max = 0 } { if ($1 > max) { max = $1 } } END { print max }')
    if [ -z "${max_core_temperature}" ] ; then
        max_core_temperature=0
    fi

    echo "${max_core_temperature}"

}

msg=$(cpu_temp)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"cpu.temp\",\
  \"value\"      : $msg,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
