#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     1800_rsync_rate_error.sh
# Revision:     1.0
# Date:         2016/12/14
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  检查本机rsync是否正常
# -------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function rsync_rate_error()
{
    local filename="/cache/cache/100M.file"
    local rsync_domain=`cat /etc/sysconfig/public_rsync |awk -F= '/RSYNC/{print $2}'`

    [ -e $filename ] || dd if=/dev/zero of=$filename bs=1M count=100
    local rsync_rate=`rsync -v --password-file=/etc/public.pass rsync://public@$rsync_domain/rsync_cache/100M.file /dev/null 2>&1 |awk '/received/{printf "%d\n",$7/1000000}'`

    if [ -n "$rsync_rate" -a $rsync_rate -ge 20 ];then
        return 0
    else
        return 1
    fi
}


#---------------------------------------------------------------------------------
# Call function
msg=$(rsync_rate_error)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "rsync.rate.error",
    "value"      : $retval,
    "counterType": "GAUGE",
    "step"       : 1800
  }
]
EOF
