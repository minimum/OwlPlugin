#!/bin/bash
#=================================
#   DESCRIPTION:多玩回源监控 
#       设备组:vfcc 腾讯小视频
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/9/22 16:31
#=================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

s01_p02_gd_http_code () {
    echo -n "["
    for IP in ${IPlist[@]}; do
        http_code=`/usr/bin/curl -s -o /dev/null -w '%{http_code}' "http://vweixinf.tc.qq.com/102/20202/snsvideodownload?filekey=30270201010420301e0201660402535a04102d833759447c302f172e83e0c106082e02030561020400&bizid=1023&hy=SZ&fileparam=302c020101042530230204883383bb020457ce7e2102024eea02031e903b02032dc6c0020422dc370a0201000400" -x $IP:80  -m 10`
        echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"http_code=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.qqcom.code\",\"value\": \"$http_code\", \"counterType\": \"GAUGE\", \"step\": 60},
    done
}
s01_p02_gd_time_total () {
        echo -n "["
        for IP in ${IPlist[@]}; do
        time_total=`/usr/bin/curl -s -o /dev/null -w '%{time_total}' "http://vweixinf.tc.qq.com/102/20202/snsvideodownload?filekey=30270201010420301e0201660402535a04102d833759447c302f172e83e0c106082e02030561020400&bizid=1023&hy=SZ&fileparam=302c020101042530230204883383bb020457ce7e2102024eea02031e903b02032dc6c0020422dc370a0201000400" -x $IP:80  -m 10`
        echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"time_total=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.qqcom.time\",\"value\": \"$time_total\", \"counterType\": \"GAUGE\", \"step\": 60},
        done
}
s01_p02_gd_speede () {
        for IP in ${IPlist[@]}; do
        speed=`/usr/bin/curl -s -o /dev/null -r 0-524288 -w '%{speed_download}' "http://vweixinf.tc.qq.com/102/20202/snsvideodownload?filekey=30270201010420301e0201660402535a04102d833759447c302f172e83e0c106082e02030561020400&bizid=1023&hy=SZ&fileparam=302c020101042530230204883383bb020457ce7e2102024eea02031e903b02032dc6c0020422dc370a0201000400" -x $IP:80  -m 10`
        value=$(echo "$speed" '*' "8" | bc)
       echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"speed=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.qqcom.speed\",\"value\": \"$value\", \"counterType\": \"GAUGE\", \"step\": 60}, 
        done
}
s01_p02_gd_ping() {
        fping -A  -u -c 10 -e ${IPlist[@]} > /tmp/fpingqq.comlog 2>&1
        for IP in ${IPlist[@]};do
            loss=$(grep $IP /tmp/fpingqq.comlog |awk '{print $5}' |awk -F '/' '{print $3}'|cut -d "%" -f 1)
         echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"loss=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.qqcom.loss\",\
        \"value\": $loss, \"counterType\": \"GAUGE\", \"step\": 60}, 
        done

        for IP in ${IPlist[@]};do
            ms=$( grep $IP /tmp/fpingqq.comlog |awk -F '/' '{print $9}') 
         echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"ms=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.qqcom.ms\",\
        \"value\": $ms, \"counterType\": \"GAUGE\", \"step\": 60},  >> /tmp/check_s01_p02.tmp
        done       
sed -ie 's/,$/]/' /tmp/check_s01_p02.tmp
cat /tmp/check_s01_p02.tmp
rm -f /tmp/check_s01_p02.tmp && rm -f /tmp/fpingqq.comlog
}
##############################################################
[ ! -x  /usr/sbin/fping ] &&  yum install -y fping
endpoint=`hostname -s`
timestamp=`date +%s`
CTL_ZJ=$(hostname -s|grep ctl-zj|wc -l)
CNC_HN=$(hostname -s|grep cnc-hn|wc -l)
CTL_GD=$(hostname -s|grep ctl-gd|wc -l)
CNC_SD=$(hostname -s|grep cnc-sd|wc -l)
if [ $CTL_ZJ -eq 1 ];then
        IPlist=(180.163.22.174 180.163.22.175 180.163.22.176 180.163.22.177)
  elif [ $CNC_HN -eq 1 ];then
        IPlist=(58.250.143.102 58.250.143.47 58.250.143.44 58.250.143.24 58.250.143.29 58.250.143.103 58.250.143.45 58.250.143.46)
  elif [ $CTL_GD -eq 1 ];then
        IPlist=(183.3.229.171 183.3.229.172 183.3.229.167 183.3.252.14 183.3.252.15 183.3.229.166 183.3.229.174 183.3.229.168 \ 
183.3.229.169 183.3.229.173 183.3.229.170)
 elif [ $CNC_SD -eq 1 ];then
       IPlist=(58.247.204.73 58.247.204.103 58.247.204.102 58.247.204.104 223.167.87.124 223.167.87.125 58.247.204.101 58.247.204.70
58.247.204.115 58.247.204.71 58.247.204.72 58.247.204.74 58.247.204.105 223.167.87.126)
fi

if [ -f /root/sh/weiqs/weixin.txt ];then
        s01_p02_gd_time_total
        s01_p02_gd_speede
        s01_p02_gd_ping
    else
  exit;
fi
