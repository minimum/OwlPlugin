#!/bin/bash
#===============================================================================
#   DESCRIPTION:军网超父 回源网络监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/11/17 11:22
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
function fpingloss
{
        rm -f /tmp/.chinamil_fping_net.tmp
        echo -n "["
        for IP in ${CHINAMILIP[@]}
        do
    
        value=$(httping $IP -Gs -c30 -t 3 -f|tail -n2|grep connects|awk '{print $5}'|sed -e 's/%/ /')

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"军网源loss=$IP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.upstream.vv\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120},
        done
}
function fpingms
{
        for IP in ${CHINAMILIP[@]}
        do
        local value=$(httping $IP -Gs -c30 -t 3 -f|tail -n2|grep round-trip |awk -F "/" '{print $5}'|sed -e 's/ms//')

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"军网源ms=$IP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.upstream.vv\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120}, >> /tmp/.chinamil_httping_net.tmp

        done
        sed -ie 's/,$/]/' /tmp/.chinamil_httping_net.tmp
        cat /tmp/.chinamil_httping_net.tmp
        rm -f /tmp/.chinamil_httping_net.tmp
}
#####################################################
s01_p02_gd=$(/FastwebApp/fwutils/bin/fwhoami|egrep s01.p02_gd|wc -l)
if [ $s01_p02_gd -gt 0 ];then
CHINAMILIP=$(cat /opt/vfcc/nginx/conf/vhost.d/vv.chinamil.com.cn.conf |egrep fail_timeout|egrep -v "#"|awk '!a[$2]++ {print $2}'|sed -e 's/:80//')
endpoint=$(hostname -s)
timestamp=`date +%s`

fpingloss
fpingms
 else
exit;
fi
