#!/bin/bash
#=================================
#   DESCRIPTION:多玩回源监控 
#       设备组:vfcc 超父
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/8/25 16:31
#=================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

s01_p02_gd_http_code () {
    echo -n "["
    for IP in $IPlist; do
        http_code=`/usr/bin/curl -s -o /dev/null -w '%{http_code}' http://w5.dwstatic.com/do_not_delete/detect.jpg -x $IP  -m 10`
        echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"http_code=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.yycom.code\",\
         \"value\": $http_code, \"counterType\": \"GAUGE\", \"step\": 60},
    done

}
s01_p02_gd_time_total () {
        for IP in $IPlist; do
        time_total=`/usr/bin/curl -s -o /dev/null -w '%{time_total}' http://w5.dwstatic.com/do_not_delete/detect.jpg -x $IP  -m 10`
        echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"time_total=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.yycom.time\",\
         \"value\": $time_total, \"counterType\": \"GAUGE\", \"step\": 60},
        done
}
s01_p02_gd_speede () {
        for IP in $IPlist; do
        speed=`/usr/bin/curl -s -o /dev/null -r 0-524288 -w '%{speed_download}' http://w5.dwstatic.com/47/9/1614/1633289-99-1459917299.mp4 -x $IP  -m 10`
        value=$(echo "$speed" '*' "8" | bc)
       echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"speed=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.yycom.speed\",\
        \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60},  >> /tmp/check_s01_p02.tmp
        done
sed -ie 's/,$/]/' /tmp/check_s01_p02.tmp
cat /tmp/check_s01_p02.tmp
rm -f /tmp/check_s01_p02.tmp
}
##############################################################
endpoint=`hostname -s`
timestamp=`date +%s`
if [[ -f /var/named/dwstatic.com  ]]; then
	IPlist=$(cat /opt/vfcc/nginx/conf/vhost.d/*.dwstatic.com.conf|egrep fail_timeout|egrep -v "#"|awk '!a[$2]++ {print $2}')
        s01_p02_gd_http_code
        s01_p02_gd_time_total
        s01_p02_gd_speede
 else
    exit;
fi
