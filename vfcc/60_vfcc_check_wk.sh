#!/bin/bash
#===============================================================================
#   DESCRIPTION:监控vfcc worker是否僵死 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

if [ -f /etc/init.d/vfcc ];then
check_worker=`{ curl http://127.0.0.1:38888/get_taskcount & sleep 30; } &>/dev/null; kill $! &>/dev/null && echo 0 || echo 1`
else
    exit;
fi
endpoint=`hostname -s`
value=$check_worker
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"service.proc\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
