#!/bin/bash
#===============================================================================
#   DESCRIPTION:监控探测文件是否存在 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin


if [ -f /etc/init.d/vfcc ];then
bz_gif=`[ -f /wdata/dd.myapp.com/16891/bz.gif ] && echo 1 || echo 0`
else
    exit;
fi
endpoint=`hostname -s`
value=$bz_gif
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"service.file\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
