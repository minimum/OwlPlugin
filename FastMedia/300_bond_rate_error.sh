#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     300_bond_rate_error.sh
# Revision:     1.0
# Date:         2016/11/10
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  检查本机网卡绑定速率是否均衡
# -------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function bond_rate_error()
{
    bond_rate=`sar -n DEV 2 10 |grep Average|awk '/'bond0'/{printf "%d",$6*8/1000}'`
if [ -n "$bond_rate" ]&&[ $bond_rate -gt 100 ];then

    for i in `cat /proc/net/bonding/bond0|grep "Slave Interface"|awk '{print $3}'`
    do
        sar -n DEV 2 10 |grep Average|awk '/'$i'/{print $6}'
    done >>/tmp/txKB_list.txt

    max_rate=`sort -n /tmp/txKB_list.txt|tail -1`
    min_rate=`sort -n /tmp/txKB_list.txt|head -1`
    rm -f /tmp/txKB_list.txt

    if [ "$min_rate" == "0.00" ];then
        return 1
    else
        net_percent=`awk 'BEGIN{printf "%d\n", '$min_rate' / '$max_rate' * 100}'`
        [ $net_percent -lt 50 ]  && return 1 || return 0
    fi

else
    return 0
fi
}

# Call function
msg=$(bond_rate_error)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"bond.rate.error\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 300}]"
