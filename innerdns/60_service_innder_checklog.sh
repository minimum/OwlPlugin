#!/bin/bash
#===============================================================================
#   DESCRIPTION:innder DNS是否正常! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/07/22 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

function check_iner_log {
        if [ -f /data/logs/inerdns/named/queries.log ];then
                fgrep "$minutes:" /data/logs/inerdns/named/queries.log |egrep -vE 'dns-spider.ffdns.net.*.ffdns.net|dns-spider.myxns.cn.*.ffdns.net'|egrep -c  NXDOMAIN
                else
                echo "404"
        fi
}
function check_time {
        if [ -f /data/logs/inerdns/named/queries.log ];then
                fgrep -c "$minutes:" /data/logs/inerdns/named/queries.log
           else
                exit;
        fi
}

minutes=`date +"%d-%b-%Y %H:%M" --date '10 minutes ago'`
msg1=$(check_iner_log)
msg2=$(check_time)
value=$msg1
value1=$msg2
endpoint=`hostname -s`
timestamp=`date +%s`

echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \" \",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.dns.innder.logs\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60},\
        \
        {\"endpoint\": \"$endpoint\",\
        \"tags\": \" \",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.dns.innder.time\",\
        \"value\": \"$value1\", \
        \"counterType\": \"GAUGE\", \"step\": 60} \
        ]
