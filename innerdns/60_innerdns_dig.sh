#!/bin/bash


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH


function is_slave() {
    if ( grep -iqE 'KANS_TYPE.*=.*DRRS' /usr/local/innerdns/etc/innerdns.conf 2>/dev/null ); then
        return 0
    else
        return 1
    fi
}


function dig_serve_ip() {

    serve_ip=`ifconfig | grep -oP '(\d{1,3}\.){3}\d{1,3}' | head -1`
    dig_result=`dig @${serve_ip} +short +time=3 +tries=2 123wa.com | grep -vE '(^$|^;)'`

    if ( is_slave ) && [ -z "$dig_result" ]; then
        echo Error
        return 1
    else
        echo OK
        return 0
    fi
}


msg=$(dig_serve_ip)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"innerdns.dig\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
