#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     600_system_process_zombie.sh
# Revision:     1.0
# Date:         2016/08/31
# Author:       蒋彬
# Email:        jiangbin@fastweb.com.cn
# Description:  查找系统的僵尸进程数量
# -------------------------------------------------------------------------------
# Revision 1.0
# 查找系统的僵尸进程数量
# -------------------------------------------------------------------------------


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
timestamp=`date +%s`

value=`ps -A -ostat,ppid,pid,cmd | grep -e '^[Zz]'|wc -l`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"system.process.zombie\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 600}]
