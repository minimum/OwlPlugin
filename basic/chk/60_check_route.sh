#!/bin/bash
#-*-coding:utf-8-*-
# uptime: 2016-11-04-bulid
# auther：zhangyb@fastweb.com.cn

# 参数说明:
# WANIP      多线 IP
# SWITCH     开关
# NULLQUE    空队列
# SRCIP      多线 IP 数组
# ROUTETABLE 路由表个数
# PCRE       内网 IP 正则

# 导入系统环境变量
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

WANIP=""
SWITCH=true
POLICY=0
NULLQUE=()
GATEWAY=()
SRCIP=()
ROUTETABLE=()
PCRE="(172\.1[6789]\.|172\.2[0-9]\.|172\.3[01]\.|192\.168\.|10\.)"

# 检查多线机房，检查默认路由，检查路由侧路数量
# OCCURS 判断 ip ru 看到的自定义的策略路由是否为空，WANIP 多线 IP
function main() {
    INDEX=0
    ADDUP=0
    OCCNUM=$(ip ru | wc -l)
    DEFCOUNT=`ip r | grep "default via" | wc -l`
    GWIFACE=$(ip r | awk '/^default/{print $5}')
    OCCURS=$(ip ru | awk '{print $NF}'| sort -u | egrep -v '(default|main|local)')
    WANIP=$(ip r | egrep "dev ${GWIFACE}.*proto.*kernel.*src" | awk '{print $NF}' | egrep -v "^${PCRE}")

    if [ $DEFCOUNT -gt 1 ]; then
        makejson 1
    fi

    for NUMS in $WANIP
    do
        SRCIP[$INDEX]=$NUMS
        ((INDEX++))
    done

    if [ ${#SRCIP[@]} -le 1 ]; then
        if CheckDefRoute; then
            makejson 0
        else
            makejson 1
        fi
    else
        for((x=0;x<${#SRCIP[@]};x++))
        do
            for((y=1;y<=x;y++))
            do
                    network1=$(/bin/ipcalc -n ${SRCIP[$x]}/24 | awk -F"=" '{print $2}')
                    network2=$(/bin/ipcalc -n ${SRCIP[$y]}/24 | awk -F"=" '{print $2}')
                    if [ "$network1" != "$network2" ];then
                        tags=true
                    fi
            done
        done

        if ! $tags; then makejson 1; fi
    fi

    if [ "$OCCURS" != "" ] && [ $OCCNUM -gt 1000 ]; then
        for TAB in $OCCURS
        do
            ROUTETABLE[$ADDUP]=$TAB
            ((ADDUP++))
        done
        Checkrclocal
    else
        makejson 1
    fi
}

#* 检查 rc.local 中配置是否正确，使用脚本是否和 routeX.sh 一致
function Checkrclocal() {
    local CODE=$(grep -q "route${#SRCIP[@]}.sh" /etc/rc.local && echo 0 || echo 1)

    if [ $CODE != 0 ]; then
        makejson 1
    fi

    if [ ! -f /etc/route${#SRCIP[@]}.sh ]; then
        makejson 1
    fi
    Checkiprlt
}

# 检查路由表号数量，如果全为空进入添加流程，如果存在则进入检查流程
# ROUTETABLE 代表的是 tablenumber，代入 ip r l t 进行验证是否存在
function Checkiprlt() {
    local EXIST=0

    for NUM in ${ROUTETABLE[@]}
    do
        local REQ=`/sbin/ip r l t $NUM`
        if [ "$REQ" != "" ]; then
            ((EXIST++))
        fi
    done

    if [ $EXIST -eq 0 ]; then
        makejson 1
    else
        Updateroute UPCONFIG
    fi
}

# SRCIP/多线 IP UPCONFIG/ip r SUBNET/子网 GATEWAY/网关 DNAME/网卡名称 TABLE/路由表号 RULE/路由表规则
# SRCNUM/多线 IP 数量 REFRENCE/SRC 参考标准
function Updateroute() {
    local SRCNUM=${#SRCIP[@]}

    for LINE in $(seq 1 ${SRCNUM})
    do
        ERROR=0
        SUBNET=$(cat /etc/route${SRCNUM}.sh | egrep ".*(\_SUBNET\=)([0-9]){1,}" | awk -F"=" '{print $2}' | egrep -v "^#" | egrep -v "^${PCRE}" | sed -n "${LINE}p")
        GATEWAY=$(cat /etc/route${SRCNUM}.sh | egrep ".*(\_GATEWAY\=)([0-9]){1,}" | awk -F"=" '{print $2}' | egrep -v "^#" | egrep -v "^${PCRE}" | sed -n "${LINE}p")
        DNAME=$(cat /etc/route${SRCNUM}.sh | egrep ".*(\_DEVICE\=)([a-z]){1,}" | awk -F"=" '{print $2}' | egrep -v "^#" | sed -n "${LINE}p")
        TABLE=$(cat /etc/route${SRCNUM}.sh | egrep ".*(\_TABLE\=)([0-9]){1,}" | awk -F"=" '{print $2}' | egrep -v "^#" | sed -n "${LINE}p")
        RULE=$(/sbin/ip r l t $TABLE)
        for LET in $(seq 1 ${SRCNUM})
        do
            REFRENCE=`/sbin/ip r | egrep "proto.*kernel.*src" | egrep -v "^${PCRE}" | sed -n "${LET}p"`
            SRC=$(echo $REFRENCE | awk '{print $NF}')
            Judge "$REFRENCE" $SUBNET $GATEWAY $DNAME $SRC
            case $? in
                0)((POLICY++));
                    TABLE=$(cat /etc/route${SRCNUM}.sh | egrep ".*(\_TABLE\=)([0-9]){1,}" | awk -F"=" '{print $2}' | egrep -v "^#" | sed -n "${LINE}p")
                    DNAME=$(cat /etc/route${SRCNUM}.sh | egrep ".*(\_DEVICE\=)([a-z]){1,}"| awk -F"=" '{print $2}' | egrep -v "^#" | sed -n "${LINE}p")
                    CheckExtRoute "$RULE" $GATEWAY $DNAME $SUBNET $TABLE $LET
                    ;;
                1)((ERROR++))
                    ;;
            esac
        done
        if [ $ERROR -ge ${#SRCIP[@]} ]; then
            makejson 1
        fi
    done

    if ! $SWITCH && [ "$POLICY" == "$SRCNUM" ]; then
        makejson 1
    fi

    CheckDefRoute
    makejson 0
}

# 检查路由策默认路由为空 20160923
function CheckDefRoute() {
    local DEF=$(ip r | fgrep "default via" | egrep -o "([0-9]{1,3}\.){3}[0-9]{1,3}")

    if [[ ${DEF} == "" ]]; then
        makejson 1
    else
        return 0
    fi
}

# 判断已经配置的路由表规则对比 routeX.sh 是否正确
function CheckExtRoute() {
    local RULE=$1
    local GATEWAY=$2
    local DEVICE=$3

    if [ "$RULE" == "" ]; then
        makejson 1
    else
        if [[ ! "$RULE" =~ "$GATEWAY" ]] && [[ ! "$RULE" =~ "$DEVICE" ]]; then
            makejson 1
        fi
    fi
}

# 判断 subnet 是否在 ip r 中，判断网关和 subnet 是否在一个网段，判断 dev 是否在 ip r 中，判断 ip r 的 src 不是局域网
# ARG1 REFERER  参照规则
# ARG2 SUBNET   子网
# ARG3 GATEWAY  网关
# ARG4 TYPENAME 网卡名称
# ARG5 SRC      真是多线IP
function Judge() {
    local ARG1=$1
    local ARG2=$2
    local ARG3=$3
    local ARG4=$4
    local ARG5=$5
    local SNUM=${#SRCIP[@]}
    local MASK=`echo $ARG2 | awk -F"/" '{print $1}'`
    local SUBMASK=`/bin/ipcalc -m $MASK`
    local GATEMASK=`/bin/ipcalc -m $ARG3`
    local M1=`echo $MASK | tr -d "."`
    local M2=`echo $ARG3 | tr -d "."`

    if [[ "$ARG1" =~ "$ARG2" ]]; then
        if [ "$SUBMASK" == "$GATEMASK" ] && [ $M1 -lt $M2 ]; then
            if [[ "$ARG1" =~ "$ARG4" ]]; then
                if [[ ! "$ARG5" =~ "^${PCRE}.*" ]]; then
                    if [ "$ARG5" != "$ARG3" ]; then
                        return 0
                    else
                        makejson 1
                    fi
                else
                    makejson 1
                fi
            else
                makejson 1
            fi
        else
            makejson 1
        fi
    else
        return 1
    fi
}

# 拼接 json 字符串
function makejson() {

    echo $1; exit
}

# 初始化 JSON 变量
MSG=$(main)
DATE=$(date +%s)
HOST=$HOSTNAME
TAG=""

# Send JSON message
cat << EOF
[{
    "endpoint"   : "$HOST",
    "tags"       : "$TAG",
    "timestamp"  : "$DATE",
    "metric"     : "check.route",
    "value"      : "$MSG",
    "counterType": "GAUGE",
    "step"       : "60"
}]
EOF
