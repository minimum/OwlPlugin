#!/bin/bash
#=================================
#   DESCRIPTION:cpu 均衡监控
#       设备组:all cache
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/11/13 
#1209 去除cpu 单核心监控
#===============================

#导出cpu数据
#合并cpu数据并awk 计算数据的差,SI_TIME	开始计算每个cpu的si值
CHECK_CPU_SI(){
    CPU_FILE1=/tmp/cpu_${endpoint}.tmp1
    CPU_FILE2=/tmp/cpu_${endpoint}.tmp2
    CPU_LOG1=$(cat /proc/stat|grep cpu[0-9] > $CPU_FILE1)
    sleep 15
    CPU_LOG2=$(cat /proc/stat|grep cpu[0-9] > $CPU_FILE2)
    SUM_CPU=$(paste $CPU_FILE1 $CPU_FILE2 |awk '{print $1" "$12-$2" "$13-$3" "$14-$4" "$15-$5" "$16-$6" "$17-$7" "$18-$8" "}')
    SI_TIME=$(echo $SUM_CPU|grep cpu[0-9]|awk '{x=sum;for(i=1;i<=NF;i++){sum+=$i};printf "%.3f\n",  $8/sum*100}' )
    MAX=$(echo $SI_TIME|awk 'BEGIN {max = 0} {if ($1>max) max=$1 fi} END {print max}')

}
#########################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
endpoint=`hostname -s`
timestamp=`date +%s`

CHECK_CPU_SI
value=$MAX

echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"system.service.cpu.si\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}]

