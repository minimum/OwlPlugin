#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     			60_check_system_resources.sh
# Revision:     			1.0
# Date:         			2016/09/27
# Author:       			白金
# Email:        			platinum@fastweb.com.cn
# Description:  			实现proc,crontab,cpu,mount,tcp异常监控
# -------------------------------------------------------------------------------
# Revision 1.0
# 实现proc,crontab,cpu,mount,tcp异常监控
#
# 0: OK
# 1: Error
# -------------------------------------------------------------------------------


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

# crontab -l 是否有重复
function check_crontab() {
    crontab -l 2>/dev/null | grep -v '^#' | grep -v '^$' | sort | uniq -c | awk '{print $1}' | fgrep -qv 1 && return 1 || return 0
}

# mount 是否有重复加载
function check_mount() {
    (mount | awk '{print $1}' | grep '^/' | sort | uniq -c | awk '{print $1}' | fgrep -qv 1 || mount | awk '{print $3}' | sort | uniq -c | awk '{print $1}' | fgrep -qv 1) && return 1 || return 0
}

# CPU 是否工作在最高频率
function check_cpufreq() {
    for CPUFREQ in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do [ -f $CPUFREQ ] && cat $CPUFREQ; done | fgrep -qv performance && return 1 || return 0
}

# 进程数量（除内核进程）
function check_processes() {
    local MAXNUM="500"
    NUM=$(ps ax | egrep -v '([0-9][0-9] \[[a-z].*/.*\]|cronolog|rsync|\[.*\]$)' | wc -l)
    if [ $NUM -ge $MAXNUM ]; then
        return 1
    else
        return 0
    fi
}

# 检查已安装 TCP 优化的是否已正确使用
function check_tcp() {
	([ -d /appex ] && ([ "`cat /proc/net/appex/wanIf 2>/dev/null`" != "`ip r | awk '/^default/{print $5}' | head -1`" ] || [ "`cat /proc/net/appex/tcpAccEnable 2>/dev/null`" != "1" ])) && return 1 || return 0
}

# Call function
msg1=$(check_crontab)
retval1=$?
msg2=$(check_mount)
retval2=$?
msg3=$(check_cpufreq)
retval3=$?
msg4=$(check_processes)
retval4=$?
msg5=$(check_tcp)
retval5=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.crontab.repeat\",\
    \"value\"      : $retval1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.mount.repeat\",\
    \"value\"      : $retval2,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.cpureq.status\",\
    \"value\"      : $retval3,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.proc.count\",\
    \"value\"      : $retval4,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.tcp.status\",\
    \"value\"      : $retval5,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
