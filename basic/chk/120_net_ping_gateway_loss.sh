#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     120_net_ping_gateway_loss.sh
# Revision:     1.0
# Date:         2016/09/26
# Author:       蒋彬
# Email:        jiangbin@fastweb.com.cn
# Description:  采集本机到网关的ping丢包率
# -------------------------------------------------------------------------------
# Revision 1.0
# 采集本机到网关的ping丢包率
# 注意: 如果default route默认网关不存在他会直接返回丢包率100%
# -------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
gateway=`ip route | awk '/default/ { print $3 }'`
if [ "$gateway" == "" ]; then
    echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"net.ping.gateway.loss\", \"value\": 100, \"counterType\": \"GAUGE\", \"step\": 120}]
    exit 1
fi
value=`ping -c 100 $gateway | perl -ne 'print "$1" if / (\S+)% packet loss/;'`
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"net.ping.gateway.loss\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 120}]