#!/usr/bin/env perl
=comment;
2016-3-27 by JiangBin 
*********************************************************************************************************
实现功能:
ng采集io信息

*********************************************************************************************************
v1.0.0 2016-3-27 ng采集io信息
上报一块io使用率最高的硬盘记录
[{"endpoint": "host01", "tags": "", "timestamp": 1431349763, "metric": "disk.io.util.gd", "value": 0.73699999999999999, "counterType": "GAUGE", "step": 60}]
第一次运行沒有值 [{"endpoint": "ctl-zj-122-228-193-082", "tags": "", "timestamp": 1431349763, "metric": "disk.io.util.gd", "value": 0, "counterType": "GAUGE", "step": 60}]
如果遇到其他极端情况无法数据出则打印空或错误信息.owl会判断接收数值类型不一致图形就不会绘制.
=cut;

#########################################################################################################
# PACKAGE

#use lib '/ng_agent/opt/vfcc/ng_agent/pl_modules/';
#use lib '/opt/vfcc/ng_agent/pl_modules/';
use strict;
use warnings;
#use POSIX qw(strftime);
#use Common;
#use Data::Dumper;

# PACKAGE
#########################################################################################################


#########################################################################################################
# CONSTANT

# CONSTANT
#########################################################################################################


#########################################################################################################
# VARIABLE

my $IO_DATA = "/tmp/owl_max_disk_io.data";
my $IO_DATA_CPU = "/tmp/owl_max_disk_io.cpu";
my $endpoint = readpipe("hostname -s");
my $timestamp = time();

# 获取linux系统HZ
my $HZ = readpipe("cat /boot/config-`uname -r` | grep '^CONFIG_HZ=' |awk -F'=' '{print \$2}'|head -1");
# 获取cpu数量
my $cpu_num = readpipe("cat /proc/cpuinfo | grep 'processor' | wc -l ");

chomp($cpu_num,$HZ,$endpoint);


# VARIABLE
#########################################################################################################


#########################################################################################################
# ARRAY



# ARRAY
#########################################################################################################



#########################################################################################################
# HASH && DICT

my %hash_new_diskstats;
my %hash_old_diskstats;

# HASH && DICT
#########################################################################################################



#########################################################################################################
# FUNCTION

# 取文件第一行的内容,用这个函数可以实现许多功能
sub Read_first_line {
    my $file = shift @_;
    open(my $fh,,$file) or return '';
    my $line = <$fh>;
    chomp $line;
    $line =~ s/^\s+|\s+$//g;
    return $line;
}

# 读取整个文件 {
sub Read_file{
    my $file = shift @_;
    open(my $fh,,$file) or return '';
    local $/;
    my $content = <$fh>;
    return $content;
}

# 写入整个文件
sub Write_file {
    my ($file,$content) = @_;
    open(my $fh,">",$file) || die $!;
    select($fh);
    print("$content");
    select(STDOUT);
}

## print打印加\n
#sub Print_n{
    #foreach (@_) {
        #print "$_ ";
    #}
    #print "\n";
#}

# 读取运算磁盘io
sub Analyze_disk_io {
    my ($old_cpu_stat,$old_diskstats,$new_cpu_stat,$new_diskstats) = @_;
    # 转换字符串为hash
    my %hash_old_cpu_stat = String_conver_hash_cpu($old_cpu_stat);
    my %hash_old_diskstats = String_conver_hash_diskstats($old_diskstats);
    my %hash_new_cpu_stat = String_conver_hash_cpu($new_cpu_stat);
    my %hash_new_diskstats = String_conver_hash_diskstats($new_diskstats);

    #    print Dumper(\%old_cpu_stat);
    #    print Dumper(\%old_diskstats);
    #    print Dumper(\%new_cpu_stat);
    #    print Dumper(\%new_diskstats);

    # 计算deltams
    my $deltams = 1000.0 * (($hash_new_cpu_stat{'user'} + $hash_new_cpu_stat{'system'} + $hash_new_cpu_stat{'idle'} + $hash_new_cpu_stat{'iowait'}) -
        ($hash_old_cpu_stat{'user'} + $hash_old_cpu_stat{'system'} + $hash_old_cpu_stat{'idle'} + $hash_old_cpu_stat{'iowait'})) / $cpu_num / $HZ;
    #Print_n($deltams);

    # 以上次的磁盘数量为准,防止新增硬盘对不上.
    my @arr_disk_dev = keys(%hash_old_diskstats);

    # 存放所有硬盘数据,取出最大
    #my @arr_max_io;
    my $max_flag = 0;
    my $max_util = 0;

    #print $#arr_disk_dev;
    foreach my $disk_dev  (@arr_disk_dev) {
=comment;
        #print $disk_dev ."\n";
        my $rd_ios =     $hash_new_diskstats{$disk_dev}{'rd_ios'} - $hash_old_diskstats{$disk_dev}{'rd_ios'};
        my $rd_merges =  $hash_new_diskstats{$disk_dev}{'rd_merges'} - $hash_old_diskstats{$disk_dev}{'rd_merges'};
        my $rd_sec =     $hash_new_diskstats{$disk_dev}{'rd_sec'} - $hash_old_diskstats{$disk_dev}{'rd_sec'};
        my $rd_ticks =   $hash_new_diskstats{$disk_dev}{'rd_ticks'} - $hash_old_diskstats{$disk_dev}{'rd_ticks'};
        my $wr_ios =     $hash_new_diskstats{$disk_dev}{'wr_ios'} - $hash_old_diskstats{$disk_dev}{'wr_ios'};
        my $wr_merges =  $hash_new_diskstats{$disk_dev}{'wr_merges'} - $hash_old_diskstats{$disk_dev}{'wr_merges'};
        my $wr_sec = $hash_new_diskstats{$disk_dev}{'wr_sec'} - $hash_old_diskstats{$disk_dev}{'wr_sec'};
        my $wr_ticks =   $hash_new_diskstats{$disk_dev}{'wr_ticks'} - $hash_old_diskstats{$disk_dev}{'wr_ticks'};
        my $ios_pgr =    $hash_new_diskstats{$disk_dev}{'ios_pgr'} - $hash_old_diskstats{$disk_dev}{'ios_pgr'};
        my $tot_ticks =  $hash_new_diskstats{$disk_dev}{'tot_ticks'} - $hash_old_diskstats{$disk_dev}{'tot_ticks'};
        my $rq_ticks =   $hash_new_diskstats{$disk_dev}{'rq_ticks'} - $hash_old_diskstats{$disk_dev}{'rq_ticks'};

        my $rrqms =  sprintf "%.2f",$rd_merges / $deltams * 100;
        my $wrqms =  sprintf "%.2f",$wr_merges / $deltams * 100;
        my $rs =     sprintf "%.2f",$rd_ios / $deltams * 100;
        my $ws =     sprintf "%.2f",$wr_ios / $deltams * 100;
        my $rsecs =  sprintf "%.2f",$rd_sec / $deltams * 50;
        my $wsecs =  sprintf "%.2f",$wr_sec / $deltams * 50;

        my $n_ios  =  $rd_ios + $wr_ios;
        my $n_ticks = $rd_ticks + $wr_ticks;
        my $n_kbytes = ($rd_sec + $wr_sec) / 1.0;

        my $rqsize = sprintf "%.2f",$n_ios?$n_kbytes/$n_ios:0.0;
        my $qusize = sprintf "%.2f",$rq_ticks/($deltams*10);
        my $await =  sprintf "%.2f",$n_ios?$n_ticks/$n_ios:0.0;
        my $svctm =  sprintf "%.2f",$n_ios?$tot_ticks/$n_ios:0.0;
=cut;
        my $tot_ticks =  $hash_new_diskstats{$disk_dev}{'tot_ticks'} - $hash_old_diskstats{$disk_dev}{'tot_ticks'};
        my $util =   sprintf "%.2f",$tot_ticks / $deltams * 10;

        # 取util最大的硬盘数据
        #push @arr_max_io,'{"rrqms":$rrqms,"wrqms":$wrqms,"rs":$rs,"ws":$ws,"rsecs":$rsecs,"wsecs":$wsecs,"rqsize":$rqsize,"qusize":$qusize,"await":$await,"svctm":$svctm,"util":$util}';
        #push @arr_max_io,"rrqms:$rrqms,wrqms:$wrqms,rs:$rs,ws:$ws,rsecs:$rsecs,wsecs:$wsecs,rqsize:$rqsize,qusize:$qusize,await:$await,svctm:$svctm,util:$util";

        if ($util > $max_util){
            $max_util = $util;
            #$max_flag = $#arr_max_io;
        }

#        if ($util > 100){
#            print "$disk_dev rrqms\=$rrqms wrqms\=$wrqms rs\=$rs ws\=$ws rsecs\=$rsecs wsecs\=$wsecs rqsize\=$rqsize qusize\=$qusize await\=$await svctm\=$svctm util\=100\n";
#        }else {
#            print "$disk_dev rrqms\=$rrqms wrqms\=$wrqms rs\=$rs ws\=$ws rsecs\=$rsecs wsecs\=$wsecs rqsize\=$rqsize qusize\=$qusize await\=$await svctm\=$svctm util\=$util\n";
#        }
    }
    #Print_n $arr_max_io[$max_flag];
    #[{"endpoint": "host01", "tags": "", "timestamp": 1431349763, "metric": "disk.io.util.gd", "value": 0.73699999999999999, "counterType": "GAUGE", "step": 60}]
    print qq([{"endpoint": "$endpoint", "tags": "", "timestamp": $timestamp, "metric": "disk.io.util.gd", "value": $max_util, "counterType": "GAUGE", "step": 60}]);
}

# 转换diskstats字符串为hash
sub String_conver_hash_diskstats {
    my $string = shift @_;
    my %hash;
    foreach my $char (split /\n/, $string) {
        if ($char =~ / (\D+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?)$/) {
            # Print_n $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12;
            #        sda 31983 6167 630167 263843 18638 42785 491214 1406837 0 357826 1670681
            $hash{$1} = { 'rd_ios' => $2, 'rd_merges' => $3, 'rd_sec' => $4, 'rd_ticks' => $5, 'wr_ios' => $6,
                'wr_merges'        => $7,
                'wr_sec'           => $8, 'wr_ticks' => $9, 'ios_pgr' => $10, 'tot_ticks' => $11, 'rq_ticks' => $12 };
        }
    }
    #print Dumper(\%hash);
    return %hash;
}

# 转换cpu_stat字符串为hash
sub String_conver_hash_cpu {
    my $string = shift @_;
    my %hash;
    #        #head -1 /proc/stat
    #        ##        user     nice   system      idle       iowait    irq    softirq  stealstolen  guest
    #        #2.6.18
    #        #array
    #        #1        2          3       4         5           6        7       8        9          10
    #        #cpu     12096       0     1623      2622077     10526      9      1398      0
    #        #2.6.32
    #        #cpu    93602460   8251  43850554  70401990908   1099451   9507   6037344    0           0
    foreach my $char (split /\n/, $string) {
        if ($char =~ /^(\S+?)  (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) /) {
            #Print_n $1, $2, $3, $4, $5, $6;
            $hash{'user'} = $2;
            $hash{'system'} = $4;
            $hash{'idle'} = $5;
            $hash{'iowait'} = $6;
        }
    }
    #print Dumper(\%hash);
    return %hash;
}



# FUNCTION
#########################################################################################################




#########################################################################################################
# ClASS & METHOD



# ClASS & METHOD
#########################################################################################################




############################################## MAIN START ###############################################
# MAIN

#Alert("Start io.pl");

=comment;

[root@localhost ~]# cat /proc/diskstats
   1    0 ram0 0 0 0 0 0 0 0 0 0 0 0
   1    1 ram1 0 0 0 0 0 0 0 0 0 0 0
   1    2 ram2 0 0 0 0 0 0 0 0 0 0 0
   1    3 ram3 0 0 0 0 0 0 0 0 0 0 0
   1    4 ram4 0 0 0 0 0 0 0 0 0 0 0
   1    5 ram5 0 0 0 0 0 0 0 0 0 0 0
   1    6 ram6 0 0 0 0 0 0 0 0 0 0 0
   1    7 ram7 0 0 0 0 0 0 0 0 0 0 0
   1    8 ram8 0 0 0 0 0 0 0 0 0 0 0
   1    9 ram9 0 0 0 0 0 0 0 0 0 0 0
   1   10 ram10 0 0 0 0 0 0 0 0 0 0 0
   1   11 ram11 0 0 0 0 0 0 0 0 0 0 0
   1   12 ram12 0 0 0 0 0 0 0 0 0 0 0
   1   13 ram13 0 0 0 0 0 0 0 0 0 0 0
   1   14 ram14 0 0 0 0 0 0 0 0 0 0 0
   1   15 ram15 0 0 0 0 0 0 0 0 0 0 0
   8    0 sda 12748 6693 342745 85864 5856 22219 224620 172152 0 101012 258014
   8    1 sda1 56 705 1530 512 2 0 4 56 0 459 568
   8    2 sda2 31 1244 1285 361 0 0 0 0 0 253 361
   8    3 sda3 12639 4713 339506 84878 5854 22219 224616 172096 0 100512 256972
   2    0 fd0 0 0 0 0 0 0 0 0 0 0 0
  22    0 hdc 0 0 0 0 0 0 0 0 0 0 0
   9    0 md0 0 0 0 0 0 0 0 0 0 0 0
   7    0 loop0 0 0 0 0 0 0 0 0 0 0 0
   7    1 loop1 0 0 0 0 0 0 0 0 0 0 0
   7    2 loop2 0 0 0 0 0 0 0 0 0 0 0
   7    3 loop3 0 0 0 0 0 0 0 0 0 0 0
   7    4 loop4 0 0 0 0 0 0 0 0 0 0 0
   7    5 loop5 0 0 0 0 0 0 0 0 0 0 0
   7    6 loop6 0 0 0 0 0 0 0 0 0 0 0
   7    7 loop7 0 0 0 0 0 0 0 0 0 0 0
                       对应下面
cat  /sys/block/sda/stat
   12748     6693   342745    85864     5856    22219   224620   172152        0   101012   258014
                       对应下面
The /proc/diskstats file displays the I/O statistics of block devices. Each line contains the following 14 fields:
 1 - major number
 2 - minor mumber
 3 - device name
 4 - reads completed successfully
 5 - reads merged
 6 - sectors read
 7 - time spent reading (ms)
 8 - writes completed
 9 - writes merged
10 - sectors written
11 - time spent writing (ms)
12 - I/Os currently in progress
13 - time spent doing I/Os (ms)
14 - weighted time spent doing I/Os (ms)

/proc/diskstats
主设备号  次设备号  设备名称   读完成次数   合并读完成次数   读扇区的次数  读花费的毫秒数  写完成次数  合并写完成次数   写扇区次数   写操作花费的毫秒数  正在处理的输入/输出请求数    输入/输出操作花费的毫秒数     输入/输出操作花费的加权毫秒数
8         0        sda       12748        6693            342745         85864           5856       22219            224620       172152              0                         101012                        258014

/proc/diskstats文件比/sys/block/sda/stat文件多3个域，从左至右分别对应主设备号，次设备号和设备名称。
后续的11个域在这两个文件里是相同的，它们的函义将在下面解释。除了第9个域ios_pgr，所有的域都是从启动时的累积值。

                           读完成次数   合并读完成次数   读扇区的次数  读花费的毫秒数    写完成次数   合并写完成次数    写扇区次数   写操作花费的毫秒数  正在处理的输入/输出请求数    输入/输出操作花费的毫秒数     输入/输出操作花费的加权毫秒数
/sys/block/sda/stat          arr 0          1               2             3                4          5                 6            7                8                            9                            10
                             rd_ios       rd_merges       rd_sec        rd_ticks        wr_ios     wr_merges          wr_sec      wr_ticks           ios_pgr                    tot_ticks                     rq_ticks
                             12748        6693            342745         85864           5856       22219            224620       172152              0                         101012                        258014

计算IO
deltams = 1000.0 * ((new_cpu.user + new_cpu.system + new_cpu.idle + new_cpu.iowait) - (old_cpu.user + old_cpu.system + old_cpu.idle + old_cpu.iowait)) / ncpu / HZ;
HZ = readpipe("cat /boot/config-`uname -r` | grep '^CONFIG_HZ=' |awk -F'=' '{print $2}'");

blkio.rd_ios = new_blkio.rd_ios - old_blkio.rd_ios;
blkio.rd_merges = new_blkio.rd_merges - old_blkio.rd_merges;
blkio.rd_sec = new_blkio.rd_sec - old_blkio.rd_sec;
blkio.rd_ticks = new_blkio.rd_ticks - old_blkio.rd_ticks;
blkio.wr_ios = new_blkio.wr_ios - old_blkio.wr_ios;
blkio.wr_merges = new_blkio.wr_merges - old_blkio.wr_merges;
blkio.wr_sectors = new_blkio.wr_sectors - old_blkio.wr_sectors;
blkio.wr_ticks = new_blkio.wr_ticks - old_blkio.wr_ticks;
blkio.ticks = new_blkio.ticks - old_blkio.ticks;
blkio.tot_ticks = new_blkio.tot_ticks - old_blkio.tot_ticks;
blkio.rq_ticks = new_blkio.rq_ticks - old_blkio.rq_ticks;


n_ios  = blkio.rd_ios + blkio.wr_ios;
n_ticks = blkio.rd_ticks + blkio.wr_ticks;
n_kbytes = (blkio.rd_sec + blkio.wr_sec) / 2.0;
#rd_sec和wr_sec是扇区数，如果需要换算成KB等单位，需要除以2，1KB=2*512Bytes。512Bytes为1个扇区数。
# 查看扇区数 fdisk -l
# Units = cylinders of 16065 * 512 = 8225280 bytes
# 表示每个柱面（cylinders ）的有16065个扇区，每个扇区512字节，所以每个柱面有16065*512 =8225280 bytes。

size = n_ios ? n_kbytes / n_ios : 0.0;
wait = n_ios ? n_ticks / n_ios : 0.0;
svc_t = n_ios ? blkio.ticks / n_ios : 0.0;
busy = 100.0 * blkio.ticks / deltams;
if (busy > 100.0) busy = 100.0;

参考:# http://stackoverflow.com/questions/4458183/how-the-util-of-iostat-is-computed
busy = 100.0 * blkio.ticks / deltams; /* percentage! */
if (busy > 100.0) busy = 100.0;
double deltams = 1000.0 *
        ((new_cpu.user + new_cpu.system +
          new_cpu.idle + new_cpu.iowait) -
         (old_cpu.user + old_cpu.system +
          old_cpu.idle + old_cpu.iowait)) / ncpu / HZ;
blkio.ticks = new_blkio[p].ticks - old_blkio[p].ticks

=cut;

## 读取/proc/stat,获取当前的jiffies
#my $new_cpu_stat = Read_first_line("/proc/stat");
##Printn($new_cpu_stat);

# 获取上次的jiffies
my $old_cpu_stat = Read_first_line($IO_DATA_CPU);

# 获取上次的diskstats
my $old_diskstats = Read_file($IO_DATA);

# 获取当前的jiffies
my $new_cpu_stat = Read_first_line("/proc/stat");
# 保存当前的jiffies到文件.
Write_file($IO_DATA_CPU,$new_cpu_stat);

# 获取当前的diskstats
my $new_diskstats = Read_file("/proc/diskstats");
# 保存当前的diskstats到文件.
Write_file($IO_DATA,$new_diskstats);

# 如果第一次运行获取不到上次的jiffies则先返回false
if ((!$old_cpu_stat) || (!$old_diskstats)){
    print "None";
    exit;
}



################################################################################################################

#开始分析io使用率

Analyze_disk_io($old_cpu_stat,$old_diskstats,$new_cpu_stat,$new_diskstats);









#my (@rd_ios,@rd_merges,@rd_sec,@rd_ticks,@wr_ios,@wr_merges,@wr_sec,@wr_ticks,@ios_pgr ,@tot_ticks,@rq_ticks);


## 读取 /proc/
#open(my $fh,,"/proc/diskstats") || die $!;
#
#while( <$fh> ){
#    if($_ =~ / (\D+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?) (\d+?)$/ ){
#        Print_n $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12;
#        #sda 31983 6167 630167 263843 18638 42785 491214 1406837 0 357826 1670681
#        $hash{$1}={'rd_ios'=>$2,'rd_merges'=>$3,'rd_sec'=>$4,'rd_ticks'=>$5,'wr_ios'=>$6, 'wr_merges'=>$7,
#            'wr_sec'=>$8,'wr_ticks'=>$9,'ios_pgr'=>$10,'tot_ticks'=>$11,'rq_ticks'=>$12};
#    }
#}

#print Dumper(\%hash_new_diskstats);

#my @arr_new_diskstats = keys(%hash_new_diskstats);
#my @arr_old_diskstats = keys(%hash_old_diskstats);

#foreach my $new_diskstats (@disk){
##    Print_n $disk;
##    print Dumper $hash{$disk};
#    my $rd_ios = $new_rd_ios - $old_rd_ios;
#    my $rd_merges = $new_rd_merges - $old_rd_merges;
#    my $rd_sec = $new_rd_sec - $old_rd_sec;
#    my $rd_ticks = $new_rd_ticks - $old_rd_ticks;
#    my $wr_ios = $new_wr_ios - $old_wr_ios;
#    my $wr_merges = $new_wr_merges - $old_wr_merges;
#    my $wr_sectors = $new_wr_sectors - $old_wr_sectors;
#    my $wr_ticks = $new_wr_ticks - $old_wr_ticks;
#    my $ticks = $new_ticks - $old_ticks;
#    my $tot_ticks = $new_tot_ticks - $old_tot_ticks;
#    my $rq_ticks = $new_rq_ticks - $old_rq_ticks;
#}
#
#
#my $rd_ios = $new_rd_ios - $old_rd_ios;
#my $rd_merges = $new_rd_merges - $old_rd_merges;
#my $rd_sec = $new_rd_sec - $old_rd_sec;
#my $rd_ticks = $new_rd_ticks - $old_rd_ticks;
#my $wr_ios = $new_wr_ios - $old_wr_ios;
#my $wr_merges = $new_wr_merges - $old_wr_merges;
#my $wr_sectors = $new_wr_sectors - $old_wr_sectors;
#my $wr_ticks = $new_wr_ticks - $old_wr_ticks;
#my $ticks = $new_ticks - $old_ticks;
#my $tot_ticks = $new_tot_ticks - $old_tot_ticks;
#my $rq_ticks = $new_rq_ticks - $old_rq_ticks;

# MAIN
############################################### MAIN END ################################################
























