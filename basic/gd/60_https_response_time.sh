#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
value=`curl --insecure --connect-timeout 10 -m 10 -o /dev/null -s -w %{time_total} https://127.0.0.1:443 2> /dev/null`
timestamp=`date +%s`
value=$(echo "$value*1000"|bc)
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"https.response.time\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
