#!/bin/bash
#===============================================================================
#   DESCRIPTION:lvs 是否正常! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/08/09 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

function LVS_Removing
{
	if [ -f /var/log/messages ];then	
	local value=`fgrep Keepalived /var/log/messages |egrep "$minutes"|egrep -v "VRRP_Instance|invalid passwd"|grep -c Removing`
        echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.lvs.removing\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}, 
		else
		  exit;
	fi
}

function LVS_status
{
	local IPlist=`ipvsadm -ln |grep TCP|awk '{print $2}'|egrep -w  80`	
		for VIP in $IPlist
	        do
	local value=`ipvsadm -ln -t $VIP|egrep -c Route`
	echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"lvsvip=$VIP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.lvs.status\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}, >> /tmp/checklvs.tmp	
	done
	sed -ie 's/,$/]/' /tmp/checklvs.tmp
	cat /tmp/checklvs.tmp
	rm -f /tmp/checklvs.tmp
					
}
minutes=`date +"%b %e %H:%M:"`
endpoint=`hostname -s`
timestamp=`date +%s`

if [ -f /etc/init.d/keepalived ];then
	LVS_Removing
	LVS_status
    else
	  exit;
fi
