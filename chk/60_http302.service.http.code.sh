#!/bin/bash
#===============================================================================
#   DESCRIPTION:金山云今日头条302调度监控 
#	设备组:D03.i12	
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
myip=`curl -s www.ip.cn|awk -F"：" '{ print $2 }'|awk -F" " '{ print $1 }'`
JINGSHAN=`curl -s -I -H  "Host: v6.pstatp.com"  "http://$myip/854x480.mp4?KSSAccessKeyId=qh0h9TdcEMrm1VlR2ad%2F&Expires=3583720860&Signature=V4CaGHnuGZJMagNKitsE0swDYZA%3D"|grep HTTP|awk '{print $2}'`

endpoint=`hostname -s`
value=$JINGSHAN
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"service.http.302\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
