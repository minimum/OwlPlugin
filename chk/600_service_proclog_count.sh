#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_http_response_time_sec.sh
# Revision:     1.0
# Date:         2016/08/08
# Author:       蒋彬
# Email:        jiangbin@fastweb.com.cn
# Description:  c77.i77平台专用插件,采集/data/proclog/log/pzs/目录下的access_*.log文件数量
# -------------------------------------------------------------------------------
# Revision 1.0
# c77.i77平台专用插件,采集/data/proclog/log/pzs/目录下的access_*.log文件数量
# -------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
timestamp=`date +%s`
value=`ls -l /data/proclog/log/pzs/access_*.log|wc -l`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"service.proclog.count\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 600}]

