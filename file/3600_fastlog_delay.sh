#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Found core file

# Check fastlog delay:
#   Return fastlog timestamp(int) diff to zabbix.
#       $1 - log queue file path.
function fastlog_delay() {

    local default_log_queue_path="/cache/logs/fastlog_data/upload/wait_upload_file"

    # Args Check:
    # Use default path if logpath not given:
    if [ "x${1}" = "x" ] ; then
        local log_queue_path="${default_log_queue_path}"
    else
        local log_queue_path="${1}"
    fi

    # Current Time:
    local timestamp_now=$(date +%s)

    # Check log queue file:
    if [ -f "${log_queue_path}" ] ; then

        local queue_item=$(head -1 ${log_queue_path})
        if [ -n "${queue_item}" ] ; then

            local queue_item_time_str=$(echo ${queue_item} | sed -e "s/.*-\([0-9]\{4\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\([0-9]\{2\}\).*\.tar\.gz/\1-\2-\3 \4:\5/g")
            local queue_item_timestamp=$(date +%s -d "${queue_item_time_str}")
            local timestamp_diff=$((timestamp_now - queue_item_timestamp))

            if [ ${timestamp_diff} -ge 0 ] ; then
                echo "${timestamp_diff}" | awk '{printf "%d", $1/60}'
                return 0
            else
                echo 0
                return 1
            fi

        else
            echo 0
            return 0
        fi

    else
        echo 0
        return 1
    fi

}


msg=$(fastlog_delay)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"file.fastlog_delay\",\
  \"value\"      : $msg,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 3600}]"
